/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Cotisse.DetailReservation;
import Cotisse.Paiement;
import Cotisse.Reservation;
import Cotisse.ReservationClient;
import Cotisse.Voyage;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import outils.Dbconnection;
import outils.ManipulationDate;

/**
 *
 * @author ASUS
 */

@Path("/reservation")
public class ReservationController {
    private static final Dbconnection DBCONNECTION = new Dbconnection();
   
    @GET
    @Path("/dispo/{idVoyage}/{idVehicule}")
    public Response gePlacesDispo(@PathParam(value="idVoyage") String voyage, @PathParam(value="idVehicule") String vehicule) throws SQLException{
        Reservation reservation = new Reservation();
        List<Reservation> result = new ArrayList<>();
        Connection connection = DBCONNECTION.connect();
        try{
            result = reservation.getPlacesDispo(connection, voyage, vehicule);
        }catch(SQLException ex){
            throw ex;
        }finally{
            connection.close();
        }
        //return result;
        return Response.status(200)
                .entity(new GenericEntity<List<Reservation>>(result){})
                .header("Access-Control-Allow-Origin","*")
                .header("Access-Control-Allow-Methods","GET,POST,DELETE,PUT")
                .build();
    }
    
    @POST
    @Path("/paiement")
    public Response paiement(Paiement paiement) throws SQLException, Exception{
        Connection connection = DBCONNECTION.connect();
        try{
            connection.setAutoCommit(false);
            paiement.insert(connection);
            connection.commit();
            return Response.status(200)
                .entity(new GenericEntity<String>("Demande de pièce de paiement pour validation"){})
                .header("Access-Control-Allow-Origin","*")
                .header("Access-Control-Allow-Methods","GET,POST,DELETE,PUT")
                .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Authorization, Content-Type,Accept")
                .allow("OPTIONS").build();
        }catch(SQLException ex){
            return Response.status(404)
                .entity(new GenericEntity<String>(ex.getMessage()){})
                .header("Access-Control-Allow-Origin","*")
                .header("Access-Control-Allow-Methods","GET,POST,DELETE,PUT")
                .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Authorization, Content-Type,Accept")
                .allow("OPTIONS").build();
        }finally{
            connection.rollback();
            connection.close();
        }
    }
    
    @GET
    @Path("/detail/{idResa}")
    public Response getReservationById(@PathParam(value="idResa") String id) throws SQLException{
        DetailReservation detail = new DetailReservation();
        List<DetailReservation> result = new ArrayList<>();
        Connection connection = DBCONNECTION.connect();
        try{
            result = detail.getDetailsReservation(connection, " and id like '"+id+"' and etat != 0");
        }catch(SQLException ex){
            throw ex;
        }finally{
            connection.close();
        }
        return Response.status(200)
                .entity(new GenericEntity<List<DetailReservation>>(result){})
                .header("Access-Control-Allow-Origin","*")
                .header("Access-Control-Allow-Methods","GET,POST,DELETE,PUT")
                .build();
    }
     
    @PUT
    @Path("/annuler/{id}")
    public Response annulerReservation(@PathParam(value="id") String id) throws SQLException{
        
        Connection connection = DBCONNECTION.connect();
        Reservation reservation = new Reservation(id);
        try{
            connection.setAutoCommit(false);
            reservation.updateReservation(connection, 0);
            connection.commit();
        }catch(SQLException ex){
            throw ex;
        }finally{
            connection.rollback();
            connection.close();
        }
        return Response.status(200)
                .entity(new GenericEntity<String>("Réservation annulée avec succès"){})
                .header("Access-Control-Allow-Origin","*")
                .header("Access-Control-Allow-Methods","GET,POST,DELETE,PUT")
                .allow("OPTIONS").build();
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/liste")
    public Response getReservations() throws SQLException{
        ReservationClient detail = new ReservationClient();
        List<ReservationClient> result = new ArrayList<>();
        Connection connection = DBCONNECTION.connect();
        try{
            result = detail.getPlacesDispo(connection, " and etat != 0");
        }catch(SQLException ex){
            throw ex;
        }finally{
            connection.close();
        }
        return Response.status(200)
                .entity(new GenericEntity<List<ReservationClient>>(result){})
                .header("Access-Control-Allow-Origin","*")
                .header("Access-Control-Allow-Methods","GET,POST,DELETE,PUT")
                .build();
    }
    
    @GET
    @Path("/{idUser}")
    public Response getReservations(@PathParam(value="idUser") String id) throws SQLException{
        ReservationClient detail = new ReservationClient();
        List<ReservationClient> result = new ArrayList<>();
        Connection connection = DBCONNECTION.connect();
        try{
            result = detail.getPlacesDispo(connection, " and client like '"+id+"' and etat != 0");
        }catch(SQLException ex){
            throw ex;
        }finally{
            connection.close();
        }
        return Response.status(200)
                .entity(new GenericEntity<List<ReservationClient>>(result){})
                .header("Access-Control-Allow-Origin","*")
                .header("Access-Control-Allow-Methods","GET,POST,DELETE,PUT")
                .build();
    }
    
    @GET
    @Path("/details/{idUser}/{idVoyage}")
    public Response getResaByVoyage(@PathParam(value="idUser") String client, @PathParam(value="idVoyage") String idVoyage) throws SQLException{
        DetailReservation detail = new DetailReservation();
        List<DetailReservation> result = new ArrayList<>();
        Connection connection = DBCONNECTION.connect();
        try{
            result = detail.getDetailsReservation(connection, " and client like '"+client+"' and idvoyage like '"+idVoyage+"' and etat != 0");
        }catch(SQLException ex){
            throw ex;
        }finally{
            connection.close();
        }
        return Response.status(200)
                .entity(new GenericEntity<List<DetailReservation>>(result){})
                .header("Access-Control-Allow-Origin","*")
                .header("Access-Control-Allow-Methods","GET,POST,DELETE,PUT")
                .build();
    }
    
    @POST
    @Path("/ajout")
    public Response ajouterReservation(Reservation reservation) throws Exception{
        //Vérifier si le vehicule correspond au voyage
        
        //Utilitaire transformant String to Date
        Connection connection = DBCONNECTION.connect();
        try{
            connection.setAutoCommit(false);
            String places = reservation.getPlace();
            reservation.setDateReservation(ManipulationDate.getDateNow());
            Voyage voyage = reservation.getVoyage(connection);
            places = places.trim();
            String[] place = new String[0];
            if(places.contains(",")){
                place = places.split(",");
                int i = 0;
                while(i<place.length){
                    reservation.setPlace(place[i]);
                    reservation.verifIntegritePlace(connection, voyage);
                    reservation.insertReservation(connection);
                    i++;
                }
            }else{
                reservation.insertReservation(connection);
            }
            connection.commit();
            return Response.status(200)
                .entity(new GenericEntity<String>("Réservation enregistrée"){})
                .header("Access-Control-Allow-Origin","*")
                .header("Access-Control-Allow-Methods","GET,POST,DELETE,PUT")
                .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Authorization, Content-Type,Accept")
                .allow("OPTIONS").build();
        }catch(Exception ex){
            throw ex;
            /*return Response.status(404)
                .entity(new GenericEntity<String>(ex.getMessage()){})
                .header("Access-Control-Allow-Origin","*")
                .header("Access-Control-Allow-Methods","GET,POST,DELETE,PUT")
                .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Authorization, Content-Type,Accept")
                .allow("OPTIONS").build();*/
        }finally{
            connection.rollback();
            connection.close();
        }
    }
    
}
