/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Cotisse.Token;
import Cotisse.Trajet;
import exceptions.TokenException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;
import outils.Dbconnection;

/**
 *
 * @author ASUS
 */
@Path("/trajet")
public class TrajetController {
    private static final Dbconnection DBCONNECTION = new Dbconnection();
    
    @GET
    @Path("/stat")
    public Response getResaTrajets() throws SQLException{
        Trajet trajet = new Trajet();
        List<Trajet> result = new ArrayList<Trajet>();
        Connection connection = DBCONNECTION.connect();
        try{
            result = trajet.getReservationTrajet(connection);
        }catch(Exception ex){
            throw ex;
        }finally{
            connection.close();
        }
        return Response.ok()
                .entity(new GenericEntity<List<Trajet>>(result){})
                .header("Access-Control-Allow-Origin","*")
                .header("Access-Control-Allow-Methods","GET,POST,DELETE,PUT")
                .build();
    }
    
    @GET
    @Path("/liste")
    public Response getTrajets() throws SQLException{
        Trajet trajet = new Trajet();
        List<Trajet> result = new ArrayList<Trajet>();
        Connection connection = DBCONNECTION.connect();
        try{
            result = trajet.getTrajets(connection);
        }catch(SQLException ex){
            throw ex;
        }finally{
            connection.close();
        }
        return Response.ok()
                .entity(new GenericEntity<List<Trajet>>(result){})
                .header("Access-Control-Allow-Origin","*")
                .header("Access-Control-Allow-Methods","GET,POST,DELETE,PUT")
                .build();
    }
    
    @POST
    @Path("/ajout")
    public Response ajouterTrajet(Trajet trajet) throws Exception{
        //Utilitaire transformant String to Date
        Connection connection = DBCONNECTION.connect();
        try{
            connection.setAutoCommit(false);

            trajet.insert(connection);
            connection.commit();
            return Response.status(200)
                .entity(new GenericEntity<String>("Nouveau trajet enregistré"){})
                .header("Access-Control-Allow-Origin","*")
                .header("Access-Control-Allow-Methods","GET,POST,DELETE,PUT")
                .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Authorization, Content-Type,Accept")
                .allow("OPTIONS").build();
        }
        catch(Exception ex){
            ex.printStackTrace();
            System.out.println(ex);
            return Response.status(404)
                .entity(new GenericEntity<String>(ex.getMessage()){})
                .header("Access-Control-Allow-Origin","*")
                .header("Access-Control-Allow-Methods","GET,POST,DELETE,PUT")
                .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Authorization, Content-Type,Accept")
                .allow("OPTIONS").build();
        }finally{
            connection.rollback();
            connection.close();
        }
    }
    
}
