/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Cotisse.DetailVoyage;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;
import outils.Dbconnection;

/**
 *
 * @author ASUS
 */

@Path("/recherche")
public class RechercheController {
    private static final Dbconnection DBCONNECTION = new Dbconnection();
    
    @GET
    @Path("/voyage/{depart}/{arrivee}/{date}/{nb}")
    public Response recherche(@PathParam(value="depart") String lieuDepart, @PathParam(value="arrivee") String lieuArrive, @PathParam(value="date") String dateVoyage, @PathParam(value="nb") String nombre) throws SQLException {
        Connection connection = DBCONNECTION.connect();
        try{
            DetailVoyage detail = new DetailVoyage(lieuDepart, lieuArrive, dateVoyage);
            int nb = Integer.parseInt(nombre);
            List<DetailVoyage> results = detail.recherche(connection, nb);
            return Response.status(200)
                .entity(new GenericEntity<List<DetailVoyage>>(results){})
                .header("Access-Control-Allow-Origin","*")
                .header("Access-Control-Allow-Methods","GET,POST,DELETE,PUT")
                .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Authorization, Content-Type,Accept")
                .allow("OPTIONS").build();
        }catch(Exception ex){
            ex.printStackTrace();
            /*return Response.status(500)
                .entity(new GenericEntity<String>(ex.getMessage()){})
                .header("Access-Control-Allow-Origin","*")
                .header("Access-Control-Allow-Methods","GET,POST,DELETE,PUT")
                .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Authorization, Content-Type,Accept")
                .allow("OPTIONS").build();*/
        }finally{
            connection.close();
        }
        return null;
    }
}
