package controller;

import Cotisse.Token;
import Cotisse.Utilisateur;
import com.google.gson.Gson;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import outils.Dbconnection;
import java.sql.Connection;
import javax.ws.rs.POST;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;
import outils.DaoService;

@Path("/user")
public class UserController {

    private static final Dbconnection DBCONNECTION = new Dbconnection();
    
    @GET
    @Path("/msg")
    public String getMsg() {
        return "Hello World !! - Jersey 2";
    }

    @POST
    @Path("/inscription")
    public Response inscription(Utilisateur user) throws Exception{
        Connection connection = DBCONNECTION.connect();
        try{
            user.setDaty(user.getDaty());
            user.setContact(user.getContact());
            user.setEmail(user.getEmail());
            String id = "U"+DaoService.getSequence(connection, "sequenceuser");
            user.setId(id);
            Token token = user.inscription(connection);
            
            //retouner le token
            return Response.status(200)
                .entity(new GenericEntity<Token>(token){})
                .header("Access-Control-Allow-Origin","*")
                .header("Access-Control-Allow-Methods","GET,POST,DELETE,PUT")
                .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Authorization, Content-Type,Accept")
                .build();
        }catch(Exception ex){
            //throw ex;
            return Response.status(404)
                .entity(new GenericEntity<String>(ex.getMessage()){})
                .header("Access-Control-Allow-Origin","*")
                .header("Access-Control-Allow-Methods","GET,POST,DELETE,PUT")
                .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Authorization, Content-Type,Accept")
                .build();
        }finally{
            connection.close();
        }
    }
    
    @POST
    @Path("/connect")
    //@Produces(MediaType.APPLICATION_JSON)
    public Response connect(Utilisateur user) throws Exception{
        Connection connection = DBCONNECTION.connect();
        try{
            user = user.connectionUser(connection);
            Token token = user.generateToken(connection, user.getId());
            //retouner le token
            return Response.status(200)
                .entity(new GenericEntity<Token>(token){})
                .header("Access-Control-Allow-Origin","*")
                .header("Access-Control-Allow-Methods","GET,POST,DELETE,PUT")
                .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Authorization, Content-Type,Accept")
                .build();
        }catch(Exception ex){
            return Response.status(404)
                .entity(new GenericEntity<String>(ex.getMessage()){})
                .header("Access-Control-Allow-Origin","*")
                .header("Access-Control-Allow-Methods","GET,POST,DELETE,PUT")
                .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Authorization, Content-Type,Accept")
                .build();
        }finally{
            connection.close();
        }
    }	
}
