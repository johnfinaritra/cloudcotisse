/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Cotisse.Chauffeur;
import Cotisse.Extras;
import Cotisse.Vehicule;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;
import outils.Dbconnection;

/**
 *
 * @author ASUS
 */

@Path("/infos")
public class InfosController {
    private static final Dbconnection DBCONNECTION = new Dbconnection();
    
    @GET
    @Path("/listechauffeurs")
    public Response getChauffeurs() throws SQLException{
        Chauffeur chauffeur = new Chauffeur();
        List<Chauffeur> result = new ArrayList<>();
        Connection connection = DBCONNECTION.connect();
        try{
            result = chauffeur.getChauffeurs(connection);
        }catch(Exception ex){
            throw ex;
        }finally{
            connection.close();
        }
        return Response.status(200)
                .entity(new GenericEntity<List<Chauffeur>>(result){})
                .header("Access-Control-Allow-Origin","*")
                .header("Access-Control-Allow-Methods","GET,POST,DELETE,PUT")
                .build();
    }
    
    @GET
    @Path("/listevehicules")
    public Response getVehicules() throws SQLException{
        Vehicule vehicule = new Vehicule();
        List<Vehicule> result = new ArrayList<>();
        Connection connection = DBCONNECTION.connect();
        try{
            result = vehicule.getVehicules(connection);
        }catch(Exception ex){
            throw ex;
        }finally{
            connection.close();
        }
        return Response.status(200)
                .entity(new GenericEntity<List<Vehicule>>(result){})
                .header("Access-Control-Allow-Origin","*")
                .header("Access-Control-Allow-Methods","GET,POST,DELETE,PUT")
                .build();
    }
    
    @GET
    @Path("/extras")
    public Response getExtras() throws SQLException{
        Extras extra = new Extras();
        List<Extras> result = new ArrayList<>();
        Connection connection = DBCONNECTION.connect();
        try{
            result = extra.getExtras(connection);
        }catch(SQLException ex){
            throw ex;
        }finally{
            connection.close();
        }
        return Response.status(200)
                .entity(new GenericEntity<List<Extras>>(result){})
                .header("Access-Control-Allow-Origin","*")
                .header("Access-Control-Allow-Methods","GET,POST,DELETE,PUT")
                .build();
    }
    
    @POST
    @Path("/ajoutvehicule")
    public Response ajouterVehicule(Vehicule vehicule) throws Exception{
        //Utilitaire transformant String to Date
        Connection connection = DBCONNECTION.connect();
        try{
            connection.setAutoCommit(false);
            vehicule.insert(connection);
            connection.commit();
            return Response.status(200)
                .entity(new GenericEntity<String>("Véhicule ajouté"){})
                .header("Access-Control-Allow-Origin","*")
                .header("Access-Control-Allow-Methods","GET,POST,DELETE,PUT")
                .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Authorization, Content-Type,Accept")
                .allow("OPTIONS").build();
        }catch(Exception ex){
            return Response.status(404)
                .entity(new GenericEntity<String>(ex.getMessage()){})
                .header("Access-Control-Allow-Origin","*")
                .header("Access-Control-Allow-Methods","GET,POST,DELETE,PUT")
                .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Authorization, Content-Type,Accept")
                .allow("OPTIONS").build();
        }finally{
            connection.rollback();
            connection.close();
        }
    }
    
    @POST
    @Path("/ajoutextras")
    public Response ajouterExtras(Extras extra) throws Exception{
        //Utilitaire transformant String to Date
        Connection connection = DBCONNECTION.connect();
        try{
            connection.setAutoCommit(false);
            extra.insert(connection);
            connection.commit();
            return Response.status(200)
                .entity(new GenericEntity<String>("Extra ajouté"){})
                .header("Access-Control-Allow-Origin","*")
                .header("Access-Control-Allow-Methods","GET,POST,DELETE,PUT")
                .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Authorization, Content-Type,Accept")
                .allow("OPTIONS").build();
        }catch(Exception ex){
            return Response.status(404)
                .entity(new GenericEntity<String>(ex.getMessage()){})
                .header("Access-Control-Allow-Origin","*")
                .header("Access-Control-Allow-Methods","GET,POST,DELETE,PUT")
                .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Authorization, Content-Type,Accept")
                .allow("OPTIONS").build();
        }finally{
            connection.rollback();
            connection.close();
        }
    }
    
    @PUT
    @Path("/modifier/{nom}/{id}/{tarif}")
    public Response modifExtras(@PathParam(value="id") String id, @PathParam(value="tarif") String prix, @PathParam(value="nom") String nom) throws Exception{
        double tarif = Double.parseDouble(prix);
        Extras extra = new Extras(id, nom, tarif);
        Connection connection =  DBCONNECTION.connect();
        try{
            connection.setAutoCommit(false);
            extra.updateExtra(connection);
            connection.commit();
        }catch(SQLException ex){
            throw ex;
        }finally{
            connection.rollback();
            connection.close();
        }
        return null;
    }
}
