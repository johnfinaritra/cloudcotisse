/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Cotisse.Token;
import exceptions.TokenException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;
import outils.Dbconnection;

/**
 *
 * @author ASUS
 */
@Provider
@Priority(Priorities.AUTHENTICATION)
public class GestionToken implements ContainerRequestFilter{

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        // Get the Authorization header from the request
        String method = requestContext.getMethod();
        String authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
        UriInfo url = requestContext.getUriInfo();
        String path = url.getPath();
        String connectionUser = "/user";
        
        if(!path.contains(connectionUser) && method.compareToIgnoreCase("get")!=0){
            if(authorizationHeader!=null && authorizationHeader.contains("Bearer")){
                authorizationHeader = authorizationHeader.trim().replace("Bearer", "");

                Token token = new Token(authorizationHeader);
                Dbconnection DbCon = new Dbconnection();
                Connection connection = DbCon.connect();
                try{
                    token.verifValidity(connection);
                }catch(TokenException ex){
                    requestContext.abortWith(
                    Response.status(Response.Status.UNAUTHORIZED)
                            .entity(new GenericEntity<String>(ex.getMessage()){})
                            .build());
                }catch(NullPointerException nullEx) {
                    requestContext.abortWith(
                    Response.status(Response.Status.UNAUTHORIZED)
                            .entity(new GenericEntity<String>("Token inexistant"){})
                            .build());
                }
                catch (Exception ex) {
                    Logger.getLogger(GestionToken.class.getName()).log(Level.SEVERE, null, ex);
                }finally{
                    try {
                        connection.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(GestionToken.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }
    
}
