/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Cotisse.DetailVoyage;
import Cotisse.Voyage;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;
import outils.Dbconnection;
import outils.ManipulationDate;

/**
 *
 * @author ASUS
 */

@Path("/voyage")
public class VoyageController {
    private static final Dbconnection DBCONNECTION = new Dbconnection();

    @PUT
    @Path("/annuler/{id}")
    public Response annulerVoyage(@PathParam(value="id") String id) throws SQLException{
        
        Connection connection = DBCONNECTION.connect();
        Voyage voyage = new Voyage(id);
        try{
            connection.setAutoCommit(false);
            voyage.updateVoyage(connection, 0);
            connection.commit();
        }catch(SQLException ex){
            throw ex;
        }finally{
            connection.rollback();
            connection.close();
        }
        return Response.status(200)
                .entity(new GenericEntity<String>("Voyage annulé avec succès"){})
                .header("Access-Control-Allow-Origin","*")
                .header("Access-Control-Allow-Methods","GET,POST,DELETE,PUT")
                .allow("OPTIONS").build();
    }
    
    
    @POST
    @Path("/ajout")
    public Response ajouterVoyage(Voyage voyage) throws Exception{
        //Utilitaire transformant String to Date
        Connection connection = DBCONNECTION.connect();
        try{
            voyage.setDateVoyage();
            connection.setAutoCommit(false);
            voyage.insert(connection);
            connection.commit();
            return Response.status(200)
                .entity(new GenericEntity<String>("Nouveau voyage enregistré"){})
                .header("Access-Control-Allow-Origin","*")
                .header("Access-Control-Allow-Methods","GET,POST,DELETE,PUT")
                .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Authorization, Content-Type,Accept")
                .allow("OPTIONS").build();
        }catch(SQLException ex){
            //throw ex;
            return Response.status(404)
                .entity(new GenericEntity<String>(ex.getMessage()){})
                .header("Access-Control-Allow-Origin","*")
                .header("Access-Control-Allow-Methods","GET,POST,DELETE,PUT")
                .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Authorization, Content-Type,Accept")
                .allow("OPTIONS").build();
        }finally{
            connection.rollback();
            connection.close();
        }
    }
    
    @GET
    @Path("recherche/{depart}/{arrivee}/{date}")
    public Response rechercher(@PathParam(value="depart") String lieuDepart, @PathParam(value="arrivee") String lieuArrive, @PathParam(value="date") String dateVoyage) throws SQLException, Exception{
        DetailVoyage detail = new DetailVoyage();
        List<DetailVoyage> result = new ArrayList<>();
        Connection connection = DBCONNECTION.connect();
        try{
            result = detail.getVoyages(connection, " and depart like '%"+lieuDepart+"%' and arrivee like '%"+lieuArrive+"%' and datevoyage > '"+ManipulationDate.stringToTimestamp(dateVoyage)+"'");
        }catch(SQLException ex){
            throw ex;
        }finally{
            connection.close();
        }
        //return result;
        return Response.status(200)
                .entity(new GenericEntity<List<DetailVoyage>>(result){})
                .header("Access-Control-Allow-Origin","*")
                .header("Access-Control-Allow-Methods","GET,POST,DELETE,PUT")
                .build();
    }
    
    @GET
    @Path("/liste")
    public Response getVoyages() throws SQLException{
        DetailVoyage detail = new DetailVoyage();
        List<DetailVoyage> result = new ArrayList<>();
        Connection connection = DBCONNECTION.connect();
        try{
            result = detail.getVoyages(connection, "");
        }catch(SQLException ex){
            throw ex;
        }finally{
            connection.close();
        }
        //return result;
        return Response.status(200)
                .entity(new GenericEntity<List<DetailVoyage>>(result){})
                .header("Access-Control-Allow-Origin","*")
                .header("Access-Control-Allow-Methods","GET,POST,DELETE,PUT")
                .build();
    }
}
