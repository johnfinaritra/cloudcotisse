/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package outils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author ASUS
 */
public class DaoService {
    public static int getSequence(Connection connection, String seqName)throws SQLException{
        String query = "SELECT nextval('"+seqName+"') as seq";
        PreparedStatement preparedStat = connection.prepareStatement(query);
        ResultSet resultSet = null;
        int result = 0;
        try{
                resultSet = preparedStat.executeQuery();
                while(resultSet.next()){
                    result = resultSet.getInt("seq");
                }
            }catch(SQLException e){
                throw e;
            }finally{
                preparedStat.close();
                resultSet.close();
            }
        return result;
    }
}
