/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package outils;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author ASUS
 */
public class ManipulationDate {
    public static String joinStr(String[] tab, String join) {
        String res = "";
        for (int i = 0; i < tab.length; i++) {
            res += tab[i];
            res += tab.length != i + 1 ? join : "";
        }
        return res;
    }

    public static String[] getOccurenceMatch(String str, String regex) {
        Pattern p = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        System.out.println(str + " & " + regex);
        Matcher m = p.matcher(str);
        Vector<String> liste = new Vector<String>();
        while (m.find()) {
            liste.add(m.group(0));
        }
        String[] listefin = new String[liste.size()];
        for (int i = 0; i < listefin.length; i++) {
            listefin[i] = liste.get(i);
        }
        return listefin;
    }

    public static String getQuantiemeJour(Timestamp time) {
        String[] liste = {"dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"};
        int pos = time.getDay();
        return liste[pos];
    }

    public static Date stringToDate(String date) throws Exception {
        Date result = null;
        String dateModif = "([0-9]{2}|[0-9]{4})[-\\/, .][0-9]{1,2}[-\\/, .]([0-9]{4}|[0-9]{2})";
        String partDate = "";
        String[] tmpDate = getOccurenceMatch(date, dateModif);
        if (tmpDate.length == 0) {
            throw new Exception("Date introuvable");
        } else {
            partDate = tmpDate[0];
        }

        String[] res = partDate.split("[-\\.,_/ ]");
        try {
            String tmp = joinStr(res, "-");
            result = Date.valueOf(tmp);
        } catch (Exception e) {
            try {
                String tmp2 = res[0];
                res[0] = res[2];
                res[2] = tmp2;
                String tmp = joinStr(res, "-");
                result = Date.valueOf(tmp);
            } catch (Exception ex) {
                throw new Exception("Date invalide essayer avec DD/MM/YYYY ou YYYY/MM/DD. On peut rajouter HH:MI:SS");
            }
        }

        if (result == null) {
            throw new Exception("Date invalide  essayer DD-MM-YYYY ou YYYY-MM-DD. On peut rajouter HH:MI:SS");
        }
        return result;
    }

    public static Timestamp stringToTimestamp(String date) throws Exception {
        String dateModif = "([0-9]{2}|[0-9]{4})[-\\/, .][0-9]{1,2}[-\\/, .]([0-9]{4}|[0-9]{2})";
        String heureMotif = "[0-9]{2}:[0-9]{2}:[0-9]{2}";
        String partDate = "";
        String hourPart = " 00:00:00";
        String[] tmpDate = getOccurenceMatch(date, dateModif);
        String[] tmpHour = getOccurenceMatch(date, heureMotif);
        if (tmpDate.length == 0) {
            throw new Exception("Date introuvable");
        } else {
            partDate = tmpDate[0];
        }

        if (tmpHour.length == 1) {
            //une heure trouvee
            hourPart = " " + tmpHour[0];
        }
        String[] res = partDate.split("[-\\.,_/ ]");
        Timestamp u = null; //resultat
        try {
            String tmp = joinStr(res, "-");
            u = Timestamp.valueOf(tmp + hourPart);
        } catch (Exception e) {
            try {
                String tmp2 = res[0];
                res[0] = res[2];
                res[2] = tmp2;
                String tmp = joinStr(res, "-");
                u = Timestamp.valueOf(tmp + hourPart);
            } catch (Exception e2) {
                throw new Exception("Date invalide essayer avec DD-MM-YYYY ou YYYY-MM-DD. On peut rajouter HH:MI:SS");
            }
        }

        if (u == null) {
            throw new Exception("Date invalide  essayer DD-MM-YYYY ou YYYY-MM-DD. On peut rajouter HH:MI:SS");
        }
        return u;
    }

    public static Timestamp geDatePaque(int annee) {
        // application de l algo de Butcher-Meeus
        int a = annee % 19,
                b = annee / 100,
                c = annee % 100,
                d = b / 4,
                e = b % 4,
                g = (8 * b + 13) / 25,
                h = (19 * a + b - d - g + 15) % 30,
                j = c / 4,
                k = c % 4,
                m = (a + 11 * h) / 319,
                r = (2 * e + 2 * j - k - h + m + 32) % 7,
                n = (h - m + r + 90) / 25,
                p = (h - m + r + n + 19) % 32;
        return Timestamp.valueOf(annee + "-" + n + "-" + p + " 00:00:00");
    }

    public static Timestamp addJour(Timestamp date, int n) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date.getTime());
        calendar.add(Calendar.DAY_OF_MONTH, n);
        Timestamp time = new Timestamp(calendar.getTime().getTime());
        return time;
    }

    public static Timestamp getNow() {
        Calendar calendar = Calendar.getInstance();
        Timestamp time = new Timestamp(calendar.getTime().getTime());
        return time;
    }
    
    public static Date getDateNow() {
        Calendar calendar = Calendar.getInstance();
        Date time = new Date(calendar.getTime().getTime());
        return time;
    }

    public static int diffDeuxDate(Timestamp a, Timestamp b) {
        LocalDateTime date1 = a.toLocalDateTime();
        LocalDateTime date2 = b.toLocalDateTime();
        int diff = (int) ChronoUnit.DAYS.between(date1, date2);
        return diff;
    }
}
