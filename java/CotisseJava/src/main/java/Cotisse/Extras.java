/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cotisse;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Hack
 */
public class Extras {
    String id;
    String nom;
    double tarif;
    String remarque;

    public void updateExtra(Connection connection)throws SQLException{
        String query = "update extras set nom = ?, tarif = ? where id like ?";
        PreparedStatement preparedStat = null;
        try{
            preparedStat = connection.prepareStatement(query);
            preparedStat.setString(1, this.getNom());
            preparedStat.setDouble(2, this.getTarif());
            preparedStat.setString(3, this.getId());
        }catch(SQLException ex){
            throw ex;
        }finally{
            if(preparedStat!=null){
                preparedStat.close();
            }
        }
    }
    
    public List<Extras> getExtras(Connection connection) throws SQLException{
        List<Extras> extras = new ArrayList<>();
        String query = "select * from extras";
        PreparedStatement preparedStat = connection.prepareStatement(query);
        try{
            ResultSet resultSet = preparedStat.executeQuery();
            while(resultSet.next()){
                Extras extra = new Extras();
                extra.setId(resultSet.getString("id"));
                extra.setNom(resultSet.getString("nom"));
                extra.setTarif(resultSet.getDouble("tarif"));
                extra.setRemarque(resultSet.getString("remarque"));
                extras.add(extra);
            }
            resultSet.close();
        }catch(SQLException ex){
            throw ex;
        }finally{
            preparedStat.close();
        }
        return extras;
    }
    
    public void insert(Connection connection) throws SQLException{
        String query = "insert into Extras (id, nom, tarif, remarque) values('E'||nextval('seqextra'), ?, ?, ?)";
        PreparedStatement preparedStat = connection.prepareStatement(query);
        try{
            preparedStat.setString(1, this.getNom());
            preparedStat.setDouble(2, this.getTarif());
            preparedStat.setString(3, this.getRemarque());
            preparedStat.execute();
        }catch(SQLException ex){
            throw ex;
        }finally{
            preparedStat.close();
        }
    }
    
    public Extras(String id, String nom, double tarif) {
        this.id = id;
        this.nom = nom;
        this.tarif = tarif;
    }

    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }
    

    public Extras() {
        
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public double getTarif() {
        return tarif;
    }

    public void setTarif(double tarif) {
        this.tarif = tarif;
    }
    
}
