/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cotisse;

import exceptions.TokenException;
import java.sql.Connection;
import java.sql.Timestamp;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import outils.ManipulationDate;

/**
 *
 * @author ASUS
 */
public class Token {
    String id;
    String token;
    String utilisateur;
    Timestamp expiration;

    public void verifValidity(Connection connection) throws SQLException, Exception{
        String query = "select * from Token where token like ?";
        PreparedStatement preparedStat = null;
        ResultSet resultSet = null;
        try{
            preparedStat = connection.prepareStatement(query);
            preparedStat.setString(1, this.getToken().trim());
            resultSet = preparedStat.executeQuery();
            Timestamp exp = null;
            while(resultSet.next()){
                exp = resultSet.getTimestamp("expiration");
            }
            
            Timestamp now = ManipulationDate.getNow();
            if(now.after(exp)){
                throw new TokenException("Session expiree");
            }
        }catch(SQLException ex){
            throw ex;
        }finally{
            if(resultSet!=null){
                resultSet.close();
            }
            if(preparedStat!=null){
                preparedStat.close();
            }
        }
    }
    
    public Token(String token){
        this.setToken(token);
    }
    
    public Token(String token, String utilisateur, Timestamp expiration){
        setToken(token);
        setExpiration(expiration);
        setUtilisateur(utilisateur);
    }
    public Token(){
        
    }
    
    public void insert(Connection connection) throws SQLException{
        String query = "insert into token(token, expiration, utilisateur) values( ?, ?, ?)";
        PreparedStatement preparedStat = connection.prepareStatement(query);
        try{
            preparedStat.setString(1, this.getToken());
            preparedStat.setTimestamp(2, this.getExpiration());
            preparedStat.setString(3, this.getUtilisateur());
            preparedStat.execute();
        }catch(Exception ex){
            throw ex;
        }finally{
            preparedStat.close();
        }
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Timestamp getExpiration() {
        return expiration;
    }

    public void setExpiration(Timestamp expiration) {
        this.expiration = expiration;
    }

    public String getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(String utilisateur) {
        this.utilisateur = utilisateur;
    }
    
    
}
