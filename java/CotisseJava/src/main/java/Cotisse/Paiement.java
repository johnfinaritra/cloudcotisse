/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cotisse;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Hack
 */
public class Paiement {
    String id;
    Date datePaiement;
    double montant;
    String reservation;
    String remarque;
    int etat;
    
    private double sommePaiements(List<Paiement> paiements){
        double somme = 0;
        int i = 0, size = paiements.size();
        for(i=0; i<size; i++){
            somme += paiements.get(i).getMontant();
        }
        return somme;
    }
    
    public List<Paiement> getPaiements(Connection connection, String idReservation) throws SQLException{
        List<Paiement> paiements = new ArrayList<>();
        String query = "select * from paiement where reservation like ? ";
        ResultSet resultSet = null;
        PreparedStatement preparedStat = null;
        try{
            preparedStat = connection.prepareStatement(query);
            preparedStat.setString(1, this.getReservation());
            resultSet = preparedStat.executeQuery();
            while(resultSet.next()){
                Paiement paiement = new Paiement();
                paiement.setDatePaiement(resultSet.getDate("datepaiement"));
                paiement.setReservation(resultSet.getString("reservation"));
                paiement.setMontant(resultSet.getDouble("montant"));
                paiements.add(paiement);
            }
        }catch(SQLException ex){
            throw ex;
        }finally{
            if(resultSet!=null){
                resultSet.close();
            }
            if(preparedStat!=null){
                preparedStat.close();
            }
        }
        return paiements;
    }
    
    public void insert(Connection connection) throws SQLException, Exception{
        List<Paiement> paiement = this.getPaiements(connection, this.getReservation());
        double somme = this.sommePaiements(paiement);
        Reservation resa = new Reservation(this.getReservation());
        double extras = resa.getSommeExtras(connection);
        double frais = resa.getVoyage(connection).getTarif();
        double totalApayer = extras + frais;
        if(somme + this.getMontant() > totalApayer){
            throw new Exception("Montant excedant de " + (somme-totalApayer) + " Ariary");
        }
        String query = "insert into Paiement(id, datePaiement, montant, reservation, remarque, etat) values ('P'||nextval('sequencepaiement'), ?, ?, ?, ?, ?)";
        PreparedStatement preparedStat = connection.prepareStatement(query);
        try{
            preparedStat.setDate(1, this.getDatePaiement());
            preparedStat.setDouble(2, this.getMontant());
            preparedStat.setString(3, this.getReservation()); 
            preparedStat.setString(4, this.getRemarque());
            preparedStat.setInt(5, this.getEtat());
            preparedStat.execute();
        }catch(Exception ex){
            throw ex;
        }finally{
            preparedStat.close();
        }
    }
    
    public Paiement(String id, Date datePaiement, double montant, String reservation, String remarque, int etat) {
        this.id = id;
        this.datePaiement = datePaiement;
        this.montant = montant;
        this.reservation = reservation;
        this.remarque = remarque;
        this.etat = etat;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDatePaiement() {
        return datePaiement;
    }

    public void setDatePaiement(Date datePaiement) {
        this.datePaiement = datePaiement;
    }

    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }

    public String getReservation() {
        return reservation;
    }

    public void setReservation(String reservation) {
        this.reservation = reservation;
    }

    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }
    
    private Paiement(){
        
    }
    
}
