/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cotisse;

/**
 *
 * @author Hack
 */
public class ExtraReservation {
    String idExtra;
    String idReservation;
    
    public ExtraReservation(String idExtra, String idReservation) {
        this.idExtra = idExtra;
        this.idReservation = idReservation;
    }

    public String getIdExtra() {
        return idExtra;
    }

    public void setIdExtra(String idExtra) {
        this.idExtra = idExtra;
    }

    public String getIdReservation() {
        return idReservation;
    }

    public void setIdReservation(String idReservation) {
        this.idReservation = idReservation;
    }
    
}
