/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cotisse;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class DetailReservation extends Reservation{
    String depart;
    String arrivee;
    double distance;
    Time duree;
    double tarif;
    Date dateVoyage;

    public DetailReservation() {
        
    }

    public List<DetailReservation> getDetailsReservation(Connection connection, String complement) throws SQLException{
        List<DetailReservation> details = new ArrayList<DetailReservation>();
        String query = "select * from detailreservation where datevoyage > now()" + complement;
        PreparedStatement preparedStat = connection.prepareStatement(query);
        try{
            ResultSet resultSet = preparedStat.executeQuery();
            while(resultSet.next()){
                DetailReservation detail = new DetailReservation();
                detail.setDepart(resultSet.getString("depart"));
                detail.setArrivee(resultSet.getString("arrivee"));
                detail.setDuree(resultSet.getTime("duree"));
                detail.setTarif(resultSet.getDouble("tarif"));
                detail.setDateReservation(resultSet.getDate("datereservation"));
                detail.setClient(resultSet.getString("client"));
                detail.setIdvoyage(resultSet.getString("idvoyage"));
                detail.setIdvehicule(resultSet.getString("vehicule"));
                detail.setPlace(resultSet.getString("place"));
                detail.setDateVoyage(resultSet.getDate("datevoyage"));
                detail.setId(resultSet.getString("id"));
                detail.setEtat(resultSet.getInt("etat"));
                details.add(detail);
            }
            resultSet.close();
        }catch(Exception ex){
            throw ex;
        }finally{
            preparedStat.close();
        }
        return details;
    }

    public Date getDateVoyage() {
        return dateVoyage;
    }

    public void setDateVoyage(Date dateVoyage) {
        this.dateVoyage = dateVoyage;
    }

    public String getIdvehicule() {
        return idvehicule;
    }

    public void setIdvehicule(String idvehicule) {
        this.idvehicule = idvehicule;
    }
    
    public String getDepart() {
        return depart;
    }

    public void setDepart(String depart) {
        this.depart = depart;
    }

    public String getArrivee() {
        return arrivee;
    }

    public void setArrivee(String arrivee) {
        this.arrivee = arrivee;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public Time getDuree() {
        return duree;
    }

    public void setDuree(Time duree) {
        this.duree = duree;
    }

    public double getTarif() {
        return tarif;
    }

    public void setTarif(double tarif) {
        this.tarif = tarif;
    }
    
    public DetailReservation(Date dateReservation, String client, String place, String idvoyage, String remarque) {
        super(dateReservation, client, place, idvoyage, remarque);
    }
    
}
