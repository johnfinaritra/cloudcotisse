/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cotisse;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

import java.sql.Date;
import java.sql.Timestamp;
import outils.EncryptService;
import outils.ManipulationDate;
/**
 *
 * @author Hack
 */
public class Utilisateur {
    String id;
    String nom;
    String prenoms;
    Date dateNaissance;
    public String daty;
    String sexe;
    public String email;
    String motdepasse;
    public String contact;

    public Utilisateur(String nom, String prenoms, Date dateNaissance, String sexe, String email, String motdepasse, String contact) throws Exception {
        this.setNom(nom);
        this.setPrenoms(prenoms);
        this.setDateNaissance(dateNaissance);
        this.setSexe(sexe);
        this.setEmail(email);
        this.setMotdepasse(motdepasse);
        this.setContact(contact);
    }

    public Utilisateur(String email, String password) throws Exception{
        this.setEmail(email);
        this.setMotdepasse(password);
    }

    public String getDaty() {
        return daty;
    }

    public void setDaty(String daty) throws Exception {
        this.setDateNaissance(ManipulationDate.stringToDate(daty));
    }
    
    public Utilisateur() {
        
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenoms() {
        return prenoms;
    }

    public void setPrenoms(String prenoms) {
        this.prenoms = prenoms;
    }

    public Date getDateNaissace() {
        return dateNaissance;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) throws Exception {
        //Must contains @ and .
        if(email.contains("@") && email.contains(".")){
            this.email = email;
        }else{
            throw new Exception("Email invalide");
        }
        
    }

    public String getMotdepasse() {
        //Must contains 2 numbers, 1 spec char, 8 in sum
        return motdepasse;
    }
    public void setMotdepasse(String motdepasse) throws Exception {
        this.motdepasse = EncryptService.md5Hashing(motdepasse);
    }
    public String getContact() {
        return contact;
    }

    public void setContact(String contact) throws Exception {
        boolean result = false;
        String[] phones = {"32", "33", "34"};
        String[] debuts = {"+261", "0", "261"};
        int i = 0, size = debuts.length;
        for(i=0; i<size; i++){
            if(contact.startsWith(debuts[i])){
                contact = contact.trim().substring(debuts[i].length());
            }
        }
        size = phones.length;
        for(i=0; i<size; i++){
            if(contact.length()==9 && contact.startsWith(phones[i])){
                result = true;
            }
        }
        if(!result){
            throw new Exception("Numero de telephone invalide");
        }
        this.contact = contact;
    }
    
    public Token generateToken(Connection connection, String idUser) throws SQLException, Exception{
        Timestamp now = ManipulationDate.getNow();
        Timestamp expiration = now;
        expiration.setHours(expiration.getHours()+1);
        Token token = new Token(EncryptService.md5Hashing(this.getEmail()+EncryptService.md5Hashing(now.toString())), idUser, expiration);
        token.insert(connection);
        return token;
    }
    
    public Utilisateur connectionUser(Connection connection) throws SQLException, Exception{
        Utilisateur user = new Utilisateur();
        String sql = "select * from Utilisateur where email like ? and motdepasse like ?";
        PreparedStatement preparedStat = connection.prepareStatement(sql);
        
        try{
            preparedStat.setString(1, this.getEmail());
            preparedStat.setString(2, this.getMotdepasse());
            ResultSet resultSet = preparedStat.executeQuery();
            while(resultSet.next()){
                user.setEmail(resultSet.getString("email"));
                user.setId(resultSet.getString("id"));
            }
            resultSet.close();
            if(user.getEmail()==null){
                throw new Exception("Email ou mot de passe invalide");
            }
        }
        catch(Exception e){
            throw e;
        }finally{
            preparedStat.close();
        }
        return user;
    }
    
    public void setDateNaissance(Date dateNaissance)throws Exception{
        int dateNaiss = dateNaissance.getYear();
        LocalDate now = LocalDate.now();
        int datenow = now.getYear()-1900;
        int age = datenow - dateNaiss;
        System.out.println("age " + age);
        try{
            if( age < 18){
                throw new Exception ("Vous devez avoir plus de 18 ans");
            }
            if(age > 95){
                throw new Exception("Limite d'age");
            }
        }
        catch(Exception e){
            throw e;
        }
    }
    
    public Token inscription(Connection connection)throws Exception{
        String sql = "insert into utilisateur (id, nom, prenoms, dateNaissace, sexe, email, motdepasse, contact) values(?,?,?,?,?,?,?,?)";
        PreparedStatement preparedStat = connection.prepareStatement(sql);
        try{
            preparedStat.setString(1, this.getId());
            preparedStat.setString(2, nom);
            preparedStat.setString(3, prenoms);
            preparedStat.setDate(4, dateNaissance);
            preparedStat.setString(5, sexe);
            preparedStat.setString(6, email);
            preparedStat.setString(7, motdepasse);
            preparedStat.setString(8, contact);
            preparedStat.executeQuery();
        }
        catch(SQLException e){
            throw e;
        }finally{
            preparedStat.close();
        }
        return this.generateToken(connection, this.getId());
    }
    
}
