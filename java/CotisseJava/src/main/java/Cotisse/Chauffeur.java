/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cotisse;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Hack
 */
public class Chauffeur {
    String id;
    String nom;
    String prenoms;
    Date dateNaissance;
    String contact;

     //CRUD
    public List<Chauffeur> getChauffeurs(Connection connection) throws SQLException{
        List<Chauffeur> chauffeurs = new ArrayList<>();
        String query = "select * from chauffeur";
        PreparedStatement preparedStat = connection.prepareStatement(query);
        try{
            ResultSet resultSet = preparedStat.executeQuery();
            while(resultSet.next()){
                Chauffeur chauffeur = new Chauffeur();
                chauffeur.setId(resultSet.getString("id"));
                chauffeur.setNom(resultSet.getString("nom"));
                chauffeur.setPrenoms(resultSet.getString("prenoms"));
                chauffeurs.add(chauffeur);
            }
            resultSet.close();
        }catch(Exception ex){
            throw ex;
        }finally{
            preparedStat.close();
        }
        return chauffeurs;
    }
    
    public Chauffeur(){
        
    }
    
    public Chauffeur(String id, String nom, String prenoms, Date dateNaissance, String contact) {
        this.id = id;
        this.nom = nom;
        this.prenoms = prenoms;
        this.dateNaissance = dateNaissance;
        this.contact = contact;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenoms() {
        return prenoms;
    }

    public void setPrenoms(String prenoms) {
        this.prenoms = prenoms;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }
    
}
