/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cotisse;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Hack
 */
public class Reservation {
    String id;
    Date dateReservation;
    String client;
    String place;
    String idvoyage;
    String remarque;
    String idvehicule;
    int etat;

    public List<Reservation> getPlacesDispo(Connection connection, String idVoyage, String idVehicule) throws SQLException{
        List<Reservation> places = new ArrayList<>();
        String query = "select * from place where id not in (select place from reservation where idvoyage like '"+idVoyage+"')  and vehicule like '"+idVehicule+"'";
        PreparedStatement preparedStat = connection.prepareStatement(query);
        ResultSet resultSet = null;
        try{
            resultSet = preparedStat.executeQuery();
            while(resultSet.next()){
                Reservation libre = new Reservation();
                libre.setPlace(resultSet.getString("id"));
                libre.setIdvehicule(resultSet.getString("vehicule"));
                places.add(libre);
            }
            
        }catch(Exception ex){
            throw ex;
        }finally{
            if(resultSet!=null){
                resultSet.close();
            }
            preparedStat.close();
        }
        return places;
    }
    
    //Methods
    public void verifDoublant(Connection connection) throws SQLException, Exception{
        String query = "select * from DetailReservation where idvoyage like ? and place like ? and etat = 1";
        PreparedStatement preparedStat = connection.prepareStatement(query);
        ResultSet resultSet = null;
        try{
            System.out.println(this.getIdvoyage() + " & " + this.getPlace());
            preparedStat.setString(1, this.getIdvoyage());
            preparedStat.setString(2, this.getPlace());
            resultSet = preparedStat.executeQuery();
            int i = 0;
            while(resultSet.next()){
                i++;
            }
            if(i>0){
                throw new Exception("La place " + this.getPlace() + " du vehicule " + this.getIdvehicule() + " a été reservée");
            }
        }catch(Exception ex){
            throw ex;
        }finally{
            if(resultSet!=null){
                resultSet.close();
            }
            preparedStat.close();
        }
    }
    
    //annule:0, créé:1, validé:11
    public void updateReservation(Connection connection, int newEtat) throws SQLException{
        String query = "update Reservation set etat = " + newEtat + " where id like ?";
        PreparedStatement preparedStat = connection.prepareStatement(query);
        try{
            preparedStat.setString(1, this.getId());
            preparedStat.execute();
        }catch(SQLException ex){
            throw ex;
        }finally{
            preparedStat.close();
        }
    }
    
    public void insertReservation(Connection connection) throws SQLException, Exception{
        try {
            this.verifDoublant(connection);
        } catch (Exception ex) {
            throw ex;
        }
        String query = "insert into reservation (id, dateReservation, client, place, idvoyage, remarque, etat) values ('R'||nextval('sequenceresa'), ?, ?, ?, ?, ?, ?)";
        PreparedStatement preparedStat = connection.prepareStatement(query);
        try{
            preparedStat.setDate(1, this.getDateReservation());
            preparedStat.setString(2, this.getClient());
            preparedStat.setString(3, this.getPlace());
            preparedStat.setString(4, this.getIdvoyage());
            preparedStat.setString(5, this.getRemarque());
            preparedStat.setInt(6, this.getEtat());
            preparedStat.execute();
        }catch(SQLException ex){
            throw ex;
        }finally{
            preparedStat.close();
        }
    }
    
    public double getSommeExtras(Connection connection) throws Exception{
        List<Extras> listExtras = this.getExtras(connection);
        double extras = 0;
        int i = 0, size = listExtras.size();
        for(i=0; i<size; i++){
            extras += listExtras.get(i).getTarif();
        }
        return extras;
    }
    
    public List<Extras> getExtras(Connection connection) throws SQLException{
        List<Extras> extras = new ArrayList<>();
        String query = "select * from detailExtra where idreservation like ?";
        PreparedStatement preparedStat = connection.prepareStatement(query);
        preparedStat.setString(1, this.id);
        ResultSet resultSet = preparedStat.executeQuery();
        try{
            while(resultSet.next()){
                Extras extra = new Extras();
                extra.setNom(resultSet.getString("nom"));
                extra.setTarif(resultSet.getDouble("tarif"));
                extras.add(extra);
            }
        }catch(SQLException ex){
            throw ex;
        }finally{
            resultSet.close();
            preparedStat.close();
        }
        return extras;
    }
    
    public Voyage getVoyage(Connection connection) throws SQLException{
        Voyage voyage = new Voyage();
        String query = "select * from Voyage where id like ?";
        PreparedStatement preparedStat = connection.prepareStatement(query);
        preparedStat.setString(1, this.getIdvoyage());
        ResultSet resultSet = preparedStat.executeQuery();
        try{
            while(resultSet.next()){
                voyage.setChauffeur(resultSet.getString("chauffeur"));
                voyage.setDateVoyage(resultSet.getTimestamp("datevoyage"));
                voyage.setId(resultSet.getString("id"));
                voyage.setTarif(resultSet.getDouble("tarif"));
                voyage.setTrajet(resultSet.getString("trajet"));
                voyage.setVehicule(resultSet.getString("vehicule"));
            }
        }catch(SQLException ex){
            throw ex;
        }finally{
            resultSet.close();
        }
        
        return voyage;
    }
    
    public void verifIntegritePlace(Connection connection, Voyage voyage) throws SQLException{
        String vehicule = voyage.getVehicule();
        String placeChoisie = this.getPlace();
        if(!placeChoisie.contains(vehicule)){
            throw new SQLException("La place choisie n'appartient pas a ce voyage");
        }
    }
    
    public Reservation(){
        
    }
    
    public Reservation(Date dateReservation, String client, String place, String idvoyage, String remarque) {
        this.dateReservation = dateReservation;
        this.client = client;
        this.place = place;
        this.idvoyage = idvoyage;
        this.remarque = remarque;
        this.etat = 1;
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDateReservation() {
        return dateReservation;
    }

    public void setDateReservation(Date dateReservation) {
        this.dateReservation = dateReservation;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getIdvoyage() {
        return idvoyage;
    }

    public void setIdvoyage(String idvoyage) {
        this.idvoyage = idvoyage;
    }

    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public String getIdvehicule() {
        return idvehicule;
    }

    public void setIdvehicule(String idvehicule) {
        this.idvehicule = idvehicule;
    }
    
    public Reservation(String id){
        this.setId(id);
    }
}
