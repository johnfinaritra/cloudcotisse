/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cotisse;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class ReservationClient extends DetailReservation{
    String vehicule;
    int nbPlaces;
    int capacite;
    String marque;

    public List<ReservationClient> getPlacesDispo(Connection connection, String complement) throws SQLException{
        List<ReservationClient> placesDispo = new ArrayList<>();
        String query = "select * from placesdispo where datevoyage > now() " + complement;
        PreparedStatement preparedStat = connection.prepareStatement(query);
        try{
            ResultSet resultSet = preparedStat.executeQuery();
            while(resultSet.next()){
                ReservationClient reservation = new ReservationClient();
                reservation.setArrivee(resultSet.getString("arrivee"));
                reservation.setCapacite(resultSet.getInt("capacite"));
                reservation.setClient(resultSet.getString("client"));
                reservation.setDateVoyage(resultSet.getDate("datevoyage"));
                reservation.setDepart(resultSet.getString("depart"));
                reservation.setIdvoyage(resultSet.getString("idvoyage"));
                reservation.setMarque(resultSet.getString("marque"));
                reservation.setNbPlaces(resultSet.getInt("nbplaces"));
                reservation.setEtat(resultSet.getInt("etat"));
                placesDispo.add(reservation);
            }
            resultSet.close();
        }catch(Exception ex){
            throw ex;
        }finally{
            preparedStat.close();
        }
        return placesDispo;
    }
    
    public String getVehicule() {
        return vehicule;
    }

    public void setVehicule(String vehicule) {
        this.vehicule = vehicule;
    }

    public int getNbPlaces() {
        return nbPlaces;
    }

    public void setNbPlaces(int nbPlaces) {
        this.nbPlaces = nbPlaces;
    }

    public int getCapacite() {
        return capacite;
    }

    public void setCapacite(int capacite) {
        this.capacite = capacite;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public String getDepart() {
        return depart;
    }

    public void setDepart(String depart) {
        this.depart = depart;
    }

    public String getArrivee() {
        return arrivee;
    }

    public void setArrivee(String arrivee) {
        this.arrivee = arrivee;
    }

    public Date getDateVoyage() {
        return dateVoyage;
    }

    public void setDateVoyage(Date dateVoyage) {
        this.dateVoyage = dateVoyage;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getIdvoyage() {
        return idvoyage;
    }

    public void setIdvoyage(String idvoyage) {
        this.idvoyage = idvoyage;
    }
    
    
    
}
