/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cotisse;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import outils.ManipulationDate;

/**
 *
 * @author ASUS
 */
public class DetailVoyage extends Voyage{
    String depart;
    String arrivee;
    double distance;
    Time duree;

    public List<DetailVoyage> recherche(Connection connection, int nb) throws SQLException{
        List<DetailVoyage> details = new ArrayList<DetailVoyage>();
        String query = "select voyage.datevoyage, trajet, depart, arrivee, id, tarif, chauffeur from placesreste, voyage where placesreste.idvoyage = voyage.id and datevoyage > ? and depart like ? and arrivee like ? and reste >= "+nb;
        PreparedStatement preparedStat = connection.prepareStatement(query);
        try{
            preparedStat.setTimestamp(1, this.getDateVoyage());
            preparedStat.setString(2, this.getDepart());
            preparedStat.setString(3, this.getArrivee());
            ResultSet resultSet = preparedStat.executeQuery();
            while(resultSet.next()){
                DetailVoyage detail = new DetailVoyage();
                detail.setTrajet(resultSet.getString("trajet"));
                detail.setDepart(resultSet.getString("depart"));
                detail.setArrivee(resultSet.getString("arrivee"));
                detail.setVehicule(resultSet.getString("id"));
                detail.setDateVoyage(resultSet.getTimestamp("datevoyage"));
                detail.setTarif(resultSet.getDouble("tarif"));
                detail.setChauffeur(resultSet.getString("chauffeur"));
                details.add(detail);
            }
            resultSet.close();
        }catch(SQLException ex){
            throw ex;
        }finally{
            preparedStat.close();
        }
        return details;
    }
    
    public List<DetailVoyage> getVoyages(Connection connection, String complement) throws SQLException{
        List<DetailVoyage> details = new ArrayList<DetailVoyage>();
        String query = "select * from detailvoyage where datevoyage > now()" + complement;
        PreparedStatement preparedStat = connection.prepareStatement(query);
        
        try{
            ResultSet resultSet = preparedStat.executeQuery();
            while(resultSet.next()){
                DetailVoyage detail = new DetailVoyage();
                detail.setTrajet(resultSet.getString("trajet"));
                detail.setDepart(resultSet.getString("depart"));
                detail.setArrivee(resultSet.getString("arrivee"));
                detail.setDistance(resultSet.getDouble("distance"));
                detail.setDuree(resultSet.getTime("duree"));
                detail.setVehicule(resultSet.getString("id"));
                detail.setDateVoyage(resultSet.getTimestamp("datevoyage"));
                detail.setTarif(resultSet.getDouble("tarif"));
                detail.setChauffeur(resultSet.getString("chauffeur"));
                details.add(detail);
            }
            resultSet.close();
        }catch(Exception ex){
            throw ex;
        }finally{
            preparedStat.close();
        }
        return details;
    }
    
    public DetailVoyage(String depart, String arrivee, String dateVoyage) throws Exception{
        this.setDepart(depart);
        setArrivee(arrivee);
        setDateVoyage(ManipulationDate.stringToTimestamp(dateVoyage));
    }
    
    public DetailVoyage(){
        
    }
    
    public String getDepart() {
        return depart;
    }

    public void setDepart(String depart) {
        this.depart = depart;
    }

    public String getArrivee() {
        return arrivee;
    }

    public void setArrivee(String arrivee) {
        this.arrivee = arrivee;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public Time getDuree() {
        return duree;
    }

    public void setDuree(Time duree) {
        this.duree = duree;
    }
    
}
