/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cotisse;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Hack
 */
public class Trajet {
    String id;
    String depart;
    String arrivee;
    double distance;
    Time duree;
    
    //Statistiques de nb de reservations par trajet
    Timestamp dateVoyage;
    double nombrePlace;
    String mois;
    String annee;
    
     public List<Trajet> getReservationTrajet(Connection connection) throws SQLException{
        List<Trajet> trajets = new ArrayList<Trajet>();
        String query = "select * from reservationpartrajet";
        PreparedStatement preparedStat = connection.prepareStatement(query);
        try{
            ResultSet resultSet = preparedStat.executeQuery();
            while(resultSet.next()){
                Trajet trajet = new Trajet();
                trajet.setNombrePlace(resultSet.getDouble("nombreplace"));
                trajet.setAnnee(resultSet.getString("annee"));
                trajet.setMois(resultSet.getString("mois"));
                trajet.setArrivee(resultSet.getString("arrivee"));
                trajet.setDepart(resultSet.getString("depart"));
                trajets.add(trajet);
            }
            resultSet.close();
        }catch(Exception ex){
            throw ex;
        }finally{
            preparedStat.close();
        }
        return trajets;
    }
    
    public List<Trajet> getTrajets(Connection connection) throws SQLException{
        List<Trajet> trajets = new ArrayList<Trajet>();
        String query = "select * from trajet";
        PreparedStatement preparedStat = connection.prepareStatement(query);
        try{
            ResultSet resultSet = preparedStat.executeQuery();
            while(resultSet.next()){
                Trajet trajet = new Trajet();
                trajet.setId(resultSet.getString("id"));
                trajet.setDepart(resultSet.getString("depart"));
                trajet.setArrivee(resultSet.getString("arrivee"));
                trajet.setDistance(resultSet.getDouble("distance"));
                trajet.setDuree(resultSet.getTime("duree"));
                trajets.add(trajet);
            }
            resultSet.close();
        }catch(Exception ex){
            throw ex;
        }finally{
            preparedStat.close();
        }
        return trajets;
    }
     
    public void insert(Connection connection) throws SQLException{
        String query = "insert into Trajet values ('T'||nextval('sequencetrajet'), ?, ?, ?, ?)";
        //Miantso sequence chauffeursequence
        PreparedStatement preparedStat = connection.prepareStatement(query);
        try{
            preparedStat.setString(1, this.getDepart());
            preparedStat.setString(2, this.getArrivee());
            preparedStat.setDouble(3, this.getDistance());
            preparedStat.setTime(4, this.getDuree());
            preparedStat.execute();
        }catch(SQLException ex){
            throw ex;
        }finally{
            preparedStat.close();
        }
    }
    
    public Trajet(){
        
    }
    public Trajet(String id, String depart, String arrivee, double distance, Time duree) {
        this.id = id;
        this.depart = depart;
        this.arrivee = arrivee;
        this.distance = distance;
        this.duree = duree;
    }

    public Timestamp getDateVoyage() {
        return dateVoyage;
    }

    public void setDateVoyage(Timestamp dateVoyage) {
        this.dateVoyage = dateVoyage;
    }

    public double getNombrePlace() {
        return nombrePlace;
    }

    public void setNombrePlace(double nombrePlace) {
        this.nombrePlace = nombrePlace;
    }

    public String getMois() {
        return mois;
    }

    public void setMois(String mois) {
        this.mois = mois;
    }

    public String getAnnee() {
        return annee;
    }

    public void setAnnee(String annee) {
        this.annee = annee;
    }
    
    
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDepart() {
        return depart;
    }

    public void setDepart(String depart) {
        this.depart = depart;
    }

    public String getArrivee() {
        return arrivee;
    }

    public void setArrivee(String arrivee) {
        this.arrivee = arrivee;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public Time getDuree() {
        return duree;
    }

    public void setDuree(Time duree) {
        this.duree = duree;
    }
    
}
