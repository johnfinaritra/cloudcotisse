/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cotisse;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import outils.ManipulationDate;

/**
 *
 * @author Hack
 */
public class Voyage {
    String id;
    Timestamp dateVoyage;
    String daty;
    String vehicule;
    String chauffeur;
    double tarif;
    String trajet;
    String heure;
    int etat;

    //Methods
    public void updateVoyage(Connection connection, int newEtat) throws SQLException{
        String query = "update voyage set etat = " + newEtat + " where id like ?";
        PreparedStatement preparedStat = connection.prepareStatement(query);
        try{
            preparedStat.setString(1, this.getId());
            preparedStat.execute();
        }catch(SQLException ex){
            throw ex;
        }finally{
            preparedStat.close();
        }
    }
    
    public void insert(Connection connection) throws SQLException{
        String query = "insert into Voyage(id, datevoyage, vehicule, chauffeur, tarif, trajet, etat) values('V'||nextval('sequencevoyage'), ?, ?, ?, ?, ?, 1)";
        PreparedStatement preparedStat = connection.prepareStatement(query);
        try{
            preparedStat.setTimestamp(1, this.getDateVoyage());
            preparedStat.setString(2, this.getVehicule());
            preparedStat.setString(3, this.getChauffeur());
            preparedStat.setDouble(4, this.getTarif());
            preparedStat.setString(5, this.getTrajet());
            preparedStat.execute();
        }catch(SQLException ex){
            throw ex;
        }finally{
            preparedStat.close();
        }
    }
    
    public Voyage(String id){
        this.setId(id);
    }
    
    public Voyage(String id, Timestamp dateVoyage, String vehicule, String chauffeur, double tarif, String trajet) {
        this.id = id;
        this.dateVoyage = dateVoyage;
        this.vehicule = vehicule;
        this.chauffeur = chauffeur;
        this.tarif = tarif;
        this.trajet = trajet;
    }

    public Voyage() {
        
    }

    public String getDaty() {
        return daty;
    }

    public void setDaty(String daty) throws Exception {
        this.daty = daty;
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Timestamp getDateVoyage() {
        return dateVoyage;
    }

    public void setDateVoyage() throws Exception {
        String date = daty + " " + heure;
        System.out.println(date);
        this.dateVoyage = Timestamp.valueOf(date);
    }
    
    public void setDateVoyage(Timestamp dateVoyage) {
        this.dateVoyage = dateVoyage;
    }

    public String getVehicule() {
        return vehicule;
    }

    public void setVehicule(String vehicule) {
        this.vehicule = vehicule;
    }

    public String getChauffeur() {
        return chauffeur;
    }

    public void setChauffeur(String chauffeur) {
        this.chauffeur = chauffeur;
    }

    public double getTarif() {
        return tarif;
    }

    public void setTarif(double tarif) {
        this.tarif = tarif;
    }

    public String getTrajet() {
        return trajet;
    }

    public void setTrajet(String trajet) {
        this.trajet = trajet;
    }

    public String getHeure() {
        return heure;
    }

    public void setHeure(String heure) {
        this.heure = heure;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }
    
}
