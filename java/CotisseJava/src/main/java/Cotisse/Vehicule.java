/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cotisse;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Hack
 */
public class Vehicule {
    String id;
    String marque;
    int capacite;
    
     //CRUD
    public List<Vehicule> getVehicules(Connection connection) throws SQLException{
        List<Vehicule> vehicules = new ArrayList<Vehicule>();
        String query = "select * from vehicule";
        PreparedStatement preparedStat = connection.prepareStatement(query);
        try{
            ResultSet resultSet = preparedStat.executeQuery();
            while(resultSet.next()){
                Vehicule vehicule = new Vehicule();
                vehicule.setId(resultSet.getString("id"));
                vehicule.setCapacite(resultSet.getInt("capacite"));
                vehicule.setMarque(resultSet.getString("marque"));
                vehicules.add(vehicule);
            }
            resultSet.close();
        }catch(Exception ex){
            throw ex;
        }finally{
            preparedStat.close();
        }
        return vehicules;
    }
    
    
    public void insert(Connection connection) throws SQLException, Exception{
        String query = "insert into Vehicule(id, marque, capacite) values (?, ?, ?)";
        //Miantso sequence chauffeursequence
        if(this.getCapacite()>32){
            throw new Exception("La capacite d'un vehicule est limite a 32 places");
        }
        PreparedStatement preparedStat = connection.prepareStatement(query);
        try{
            preparedStat.setString(1, this.getId());
            preparedStat.setString(2, this.getMarque());
            preparedStat.setInt(3, this.getCapacite());
            preparedStat.execute();
            int i = 0;
            for(i=0; i<capacite; i++){
                query = "insert into place values (?, ?, ?)";
                preparedStat = connection.prepareStatement(query);
                preparedStat.setString(1, this.getId()+(i+1));                
                preparedStat.setString(2, ""+(i+1));
                preparedStat.setString(3, this.getId());
                preparedStat.execute();
            }
        }catch(Exception ex){
            throw ex;
        }finally{
            preparedStat.close();
        }
    }
    
    public Vehicule(){
        
    }
    public Vehicule(String id, String marque, int capacite) {
        this.id = id;
        this.marque = marque;
        this.capacite = capacite;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public int getCapacite() {
        return capacite;
    }

    public void setCapacite(int capacite) {
        this.capacite = capacite;
    }
    
}
