
create database dbcotisse;
create role cotisserole WITH CREATEDB CREATEROLE;

create user cotisse with password 'cotisse';
GRANT all privileges ON DATABASE dbcotisse TO cotisserole;
grant cotisserole to cotisse;
set role;

--1)Role
create table Role(
	id varchar(15) primary key,
	nom varchar(30),
	etat integer
);
--create sequence roleSequence;

--2)Utilisateur
create table Utilisateur(
	id varchar(15) primary key,
	nom varchar(50),
	prenoms varchar(50),
	datenaissace date,
	sexe varchar(3),
	email varchar(150) not null unique,
	motdepasse varchar(250) not null,
	contact varchar(20) unique,
	role varchar(15),
	image varchar(50),
	foreign key(role) references Role(id)
);
create sequence sequenceuser start with 10 increment by 1;

--Token
create table Token(
	id serial primary key,
	token text,
	utilisateur varchar(15),
	expiration timestamp,
	foreign key(utilisateur) references Utilisateur(id)
);
create sequence sequencetoken;

--3)Vehicule
create table Vehicule(
	id varchar(20) primary key,
	marque varchar(50),
	capacite integer
);
--create sequence sequencevehicule start with 10 increment by 1;

--4)Chauffeur
create table Chauffeur(
	id varchar(15) primary key,
	nom varchar(50),
	prenoms varchar(50),
	datenaissance date,
	contact varchar(20) unique
);
create sequence sequencechauff start with 10 increment by 1;

--5)Trajet
create table Trajet(
	id varchar(15) primary key,
	depart varchar(50),
	arrivee varchar(50),
	distance decimal(15,2),
	duree time,
	CONSTRAINT constrTrajet UNIQUE (depart, arrivee)
);
create sequence sequencetrajet start with 5 increment by 1;

--6)Voyage
create table Voyage(
	id varchar(15) primary key,
	datevoyage timestamp,
	vehicule varchar(20),
	chauffeur varchar(15),
	tarif decimal(15,2),
	trajet varchar(15),
	etat integer,
	foreign key(trajet) references Trajet(id),
	foreign key(vehicule) references Vehicule(id),
	foreign key(chauffeur) references Chauffeur(id)
);
create sequence sequencevoyage start with 10 increment by 1;

create or replace view detailvoyage as select Trajet.id as trajet, depart, arrivee, distance, duree, Voyage.id, dateVoyage, vehicule, chauffeur, tarif, etat from Trajet JOIN Voyage ON Trajet.id = Voyage.trajet;

create table Place(
	id varchar(25) primary key, 
	numero varchar(25),
	vehicule varchar(25),
	foreign key(vehicule) references Vehicule(id)
);

--7)Reservation
create table Reservation(
	id varchar(15) primary key,
	datereservation date,
	client varchar(15),
	place varchar(15),
	idvoyage varchar(15),
	remarque varchar(250),
	etat integer,
	CONSTRAINT constrReservation UNIQUE (client, idvoyage, place),
	foreign key(idvoyage) references Voyage(id),
	foreign key(client) references Utilisateur(id),
	foreign key(place) references Place(id)
);
create sequence sequenceresa start with 20 increment by 1;
create view detailreservation as select Reservation.id, datereservation, client, place, idvoyage, remarque, reservation.etat, detailvoyage.etat as etatVoyage, depart, arrivee, distance, duree, datevoyage, vehicule, tarif, trajet from Reservation JOIN detailVoyage ON Reservation.idvoyage = detailVoyage.id;

create view placesreservation as select client, idvoyage, datevoyage, vehicule, depart, arrivee, etat, count(place) as nbplaces from detailreservation group by client, idvoyage, datevoyage, vehicule, depart, arrivee, etat;

create view placesdispo as select * from placesreservation, vehicule where placesreservation.vehicule = vehicule.id;
 
/*create view recherchedispo as select voyage.id, dateVoyage, voyage.vehicule, chauffeur, tarif, trajet, etat,  from voyage left join placesdispo on voyage.id = placesdispo.idvoyage;*/
 
create view placesreste as select idvoyage, datevoyage, vehicule, depart, arrivee, sum(nbplaces), marque, capacite-sum(nbplaces) as reste from placesdispo group by idvoyage, datevoyage, vehicule, depart, arrivee, marque, capacite;

--8)Extras
create table Extras(
	id varchar(15) primary key,
	nom varchar(50) unique,
	tarif decimal(15,2),
	remarque text
);
create sequence seqextra start with 5 increment by 1;

--9)ExtraReservation
create table ExtraReservation(
	idextra varchar(15),
	idreservation varchar(15),
	foreign key(idExtra) references Extras(id),
	foreign key(idReservation) references Reservation(id)
);
create view detailextra as select nom, tarif, idReservation from ExtraReservation JOIN Extras on ExtraReservation.idExtra = 
Extras.id;

--10)Paiement
create table Paiement(
	id varchar(15) primary key,
	datepaiement date,
	montant decimal(15,2),
	reservation varchar(15),
	remarque varchar(250),
	etat integer,
	foreign key(reservation) references Reservation(id)
);
create sequence sequencepaiement start with 1 increment by 1;

-- Fréquence de voyage par trajet entre 2 dates
create view reservationpartrajet as 

select sum(nbplaces) as nombrePlace,
(SELECT EXTRACT(MONTH FROM datevoyage)) as mois, 
(SELECT EXTRACT(YEAR FROM datevoyage)) as annee,
depart, arrivee from placesreservation group by mois, annee, depart, arrivee; 

-- Montant obtenu par trajet

-- Chiffres d'affaires annuel par trajet