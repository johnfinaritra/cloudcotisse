insert into Role values('R1', 'user', 1);
insert into Role values('R2', 'admin', 11);

----------- Utilisateur -----------------
insert into Utilisateur values('U1', 'Rajoelimahefa', 'Leong Hune Jhon', '1999-10-10', 'M', 'jhon@cotisse.mg', '4c25b32a72699ed712dfa80df77fc582', '0322512345', 'R2');
insert into Utilisateur values('U2', 'Raveloson', 'Finaritra Mickaelle', '2000-10-19', 'F', 'fy@cotisse.mg', '263ed3ca689addde40e4ee6150c611d3', '0326922773', 'R2');
insert into Utilisateur values('U3', 'Rabenanahary', 'Rojo ITU', '1980-10-20', 'M', 'p11.ituniversity@cotisse.mg', '6b79a69eb7d09788acb81710c0ac5ac7', '0341578545', 'R2');

insert into Utilisateur values('U4', 'Rakoto', 'Andry', '1989-05-15', 'M', 'andry@cotisse.mg', '1fd07199cca4ff81d01dca373c6e03a9', '0344578945', 'R1');
insert into Utilisateur values('U5', 'Rabenanahary', 'Rojo', '1980-10-20', 'M', 'rojo@cotisse.mg', '92c4cf9df4c01b73f7e757e78ae60841', '0324512378', 'R1');
insert into Utilisateur values('U6', 'Rajaona', 'Rado', '1980-10-25', 'M', 'rado@cotisse.mg', 'a18ddce0bdfadd4733e22281498a68ef', '0331289389', 'R1');
insert into Utilisateur values('U7', 'Rakotovelo', 'Hary', '1996-02-05', 'M', 'hary@cotisse.mg', '1b1464a2413c5dfbf6a472213ba535d6', '0341579845', 'R1');
insert into Utilisateur values('U8', 'Rabearivelo', 'Ny Aina', '1999-12-16', 'F', 'nyaina@cotisse.mg', 'c94a42b84f2cdd2f819463e746932805', '0331590645', 'R1');
insert into Utilisateur values('U9', 'Rakotonandrasana', 'Finaritra', '1995-10-19', 'F', 'finaritra@cotisse.mg', 'ef1a9b6abe1ca84a3460a826959a557f', '0320258943', 'R1');
insert into Utilisateur values('U10', 'Andrianjaona', 'Diary', '1986-08-06', 'F', 'diary@cotisse.mg', '23eee8e941181188cef6a1e39b129cc6', '0331457605', 'R1');

------------ Vehicules ---------------
insert into Vehicule values('3911TBA', 'Sprinter', 18);
insert into Vehicule values('8956TBB', 'Sprinter', 18);
insert into Vehicule values('1512WWT', 'Crafter', 18);
insert into Vehicule values('3214TBC', 'Sprinter', 18);
insert into Vehicule values('1502TAC', 'Sprinter', 18);


----------- Chauffeur ----------------
insert into Chauffeur values('C1', 'Randria', 'Manjaka', '1979-11-21', '0331183862');
insert into Chauffeur values('C2', 'Andrianjaka', 'Tsihary', '1992-12-01', '0321183863');
insert into Chauffeur values('C3', 'Ramorasata', 'Tsiaro', '1990-06-05', '0341183869');
insert into Chauffeur values('C4', 'Andrianjaona', 'Narovana', '1985-02-05', '0331145869');
insert into Chauffeur values('C5', 'Ravelonarivo', 'Christian', '1995-11-04', '0320383869');
insert into Chauffeur values('C6', 'Manarivo', 'Victor', '1983-12-06', '0331582865');
insert into Chauffeur values('C7', 'Andrianarisoa', 'Hugues', '1989-12-06', '0390184861');
insert into Chauffeur values('C8', 'Ranaivo', 'Manitra', '1989-06-09', '0398189561');
insert into Chauffeur values('C9', 'Randrianjatovo', 'Andry', '198-02-25', '0330184861');
insert into Chauffeur values('C10', 'Randriamanana', 'Rado', '1989-01-19', '0325184009');

----------- Trajets -------------------
insert into Trajet values('T1', 'antananarivo', 'mahajanga', 700, '12:30:00');
insert into Trajet values('T2', 'mahajanga', 'antananarivo', 700, '12:30:00');
insert into Trajet values('T3', 'antananarivo', 'toamasina', 300, '8:30:00');


----------- Voyages -----------------
insert into Voyage values('V1', '2020-01-29 06:30:00', '3911TBA', 'C1', 25000, 'T1', 1);
insert into Voyage values('V2', '2020-01-29 08:00:00', '8956TBB', 'C2', 32500, 'T1', 1);
insert into Voyage values('V3', '2020-02-06 12:00:00', '1502TAC', 'C2', 27000, 'T2', 1);
insert into Voyage values('V4', '2020-02-02 06:00:00', '3214TBC', 'C2', 33500, 'T1', 1);
insert into Voyage values('V5', '2020-02-08 20:00:00', '1512WWT', 'C2', 29000, 'T2', 1);
insert into Voyage values('V6', '2020-01-30 08:00:00', '8956TBB', 'C2', 25000, 'T1', 1);
insert into Voyage values('V7', '2020-02-02 12:00:00', '3911TBA', 'C2', 28500, 'T2', 1);
insert into Voyage values('V8', '2020-02-03 08:00:00', '3214TBC', 'C2', 29700, 'T3', 1);


----------- Place des vehicules -----------------
insert into Place values('3911TBA1', '1', '3911TBA');
insert into Place values('3911TBA2', '2', '3911TBA');
insert into Place values('3911TBA3', '3', '3911TBA');
insert into Place values('3911TBA4', '4', '3911TBA');
insert into Place values('3911TBA5', '5', '3911TBA');
insert into Place values('3911TBA6', '6', '3911TBA');
insert into Place values('3911TBA7', '7', '3911TBA');
insert into Place values('3911TBA8', '8', '3911TBA');
insert into Place values('3911TBA9', '9', '3911TBA');
insert into Place values('3911TBA10', '10', '3911TBA');
insert into Place values('3911TBA11', '11', '3911TBA');
insert into Place values('3911TBA12', '12', '3911TBA');
insert into Place values('3911TBA13', '13', '3911TBA');
insert into Place values('3911TBA14', '14', '3911TBA');
insert into Place values('3911TBA15', '15', '3911TBA');
insert into Place values('3911TBA16', '16', '3911TBA');
insert into Place values('3911TBA17', '17', '3911TBA');
insert into Place values('3911TBA18', '18', '3911TBA');

insert into Place values('8956TBB1', '1', '8956TBB');
insert into Place values('8956TBB2', '2', '8956TBB');
insert into Place values('8956TBB3', '3', '8956TBB');
insert into Place values('8956TBB4', '4', '8956TBB');
insert into Place values('8956TBB5', '5', '8956TBB');
insert into Place values('8956TBB6', '6', '8956TBB');
insert into Place values('8956TBB7', '7', '8956TBB');
insert into Place values('8956TBB8', '8', '8956TBB');
insert into Place values('8956TBB9', '9', '8956TBB');
insert into Place values('8956TBB10', '10', '8956TBB');
insert into Place values('8956TBB11', '11', '8956TBB');
insert into Place values('8956TBB12', '12', '8956TBB');
insert into Place values('8956TBB13', '13', '8956TBB');
insert into Place values('8956TBB14', '14', '8956TBB');
insert into Place values('8956TBB15', '15', '8956TBB');
insert into Place values('8956TBB16', '16', '8956TBB');
insert into Place values('8956TBB17', '17', '8956TBB');
insert into Place values('8956TBB18', '18', '8956TBB');

insert into Place values('1512WWT1', '1', '1512WWT');
insert into Place values('1512WWT2', '2', '1512WWT');
insert into Place values('1512WWT3', '3', '1512WWT');
insert into Place values('1512WWT4', '4', '1512WWT');
insert into Place values('1512WWT5', '5', '1512WWT');
insert into Place values('1512WWT6', '6', '1512WWT');
insert into Place values('1512WWT7', '7', '1512WWT');
insert into Place values('1512WWT8', '8', '1512WWT');
insert into Place values('1512WWT9', '9', '1512WWT');
insert into Place values('1512WWT10', '10', '1512WWT');
insert into Place values('1512WWT11', '11', '1512WWT');
insert into Place values('1512WWT12', '12', '1512WWT');
insert into Place values('1512WWT13', '13', '1512WWT');
insert into Place values('1512WWT14', '14', '1512WWT');
insert into Place values('1512WWT15', '15', '1512WWT');
insert into Place values('1512WWT16', '16', '1512WWT');
insert into Place values('1512WWT17', '17', '1512WWT');
insert into Place values('1512WWT18', '18', '1512WWT');

insert into Place values('3214TBC1', '1', '3214TBC');
insert into Place values('3214TBC2', '2', '3214TBC');
insert into Place values('3214TBC3', '3', '3214TBC');
insert into Place values('3214TBC4', '4', '3214TBC');
insert into Place values('3214TBC5', '5', '3214TBC');
insert into Place values('3214TBC6', '6', '3214TBC');
insert into Place values('3214TBC7', '7', '3214TBC');
insert into Place values('3214TBC8', '8', '3214TBC');
insert into Place values('3214TBC9', '9', '3214TBC');
insert into Place values('3214TBC10', '10', '3214TBC');
insert into Place values('3214TBC11', '11', '3214TBC');
insert into Place values('3214TBC12', '12', '3214TBC');
insert into Place values('3214TBC13', '13', '3214TBC');
insert into Place values('3214TBC14', '14', '3214TBC');
insert into Place values('3214TBC15', '15', '3214TBC');
insert into Place values('3214TBC16', '16', '3214TBC');
insert into Place values('3214TBC17', '17', '3214TBC');
insert into Place values('3214TBC18', '18', '3214TBC');

insert into Place values('1502TAC1', '1', '1502TAC');
insert into Place values('1502TAC2', '2', '1502TAC');
insert into Place values('1502TAC3', '3', '1502TAC');
insert into Place values('1502TAC4', '4', '1502TAC');
insert into Place values('1502TAC5', '5', '1502TAC');
insert into Place values('1502TAC6', '6', '1502TAC');
insert into Place values('1502TAC7', '7', '1502TAC');
insert into Place values('1502TAC8', '8', '1502TAC');
insert into Place values('1502TAC9', '9', '1502TAC');
insert into Place values('1502TAC10', '10', '1502TAC');
insert into Place values('1502TAC11', '11', '1502TAC');
insert into Place values('1502TAC12', '12', '1502TAC');
insert into Place values('1502TAC13', '13', '1502TAC');
insert into Place values('1502TAC14', '14', '1502TAC');
insert into Place values('1502TAC15', '15', '1502TAC');
insert into Place values('1502TAC16', '16', '1502TAC');
insert into Place values('1502TAC17', '17', '1502TAC');
insert into Place values('1502TAC18', '18', '1502TAC');

----------- Reservations------------------
insert into Reservation values('R1', '2020-01-15', 'U4', '3911TBA1', 'V1', '', 1);
insert into Reservation values('R2', '2020-01-19', 'U5', '8956TBB2', 'V2', '', 1);
insert into Reservation values('R3', '2020-01-20', 'U6', '3911TBA3', 'V1', '', 1);
insert into Reservation values('R4', '2020-01-16', 'U7', '8956TBB4', 'V2', '', 1);
insert into Reservation values('R5', '2020-01-19', 'U8', '3911TBA5', 'V1', '', 1);
insert into Reservation values('R6', '2020-01-22', 'U9', '8956TBB5', 'V2', '', 1);
insert into Reservation values('R7', '2020-01-23', 'U10', '3911TBA6', 'V1', '', 1);
insert into Reservation values('R8', '2020-01-15', 'U4', '8956TBB7', 'V2', '', 1);
insert into Reservation values('R9', '202-01-16', 'U5', '8956TBB8', 'V2', '', 1);
insert into Reservation values('R10', '2020-01-19', 'U6', '3911TBA9', 'V1', '', 1);
insert into Reservation values('R11', '2020-01-20', 'U7', '8956TBB10', 'V2', '', 1);
insert into Reservation values('R12', '2020-01-20', 'U8', '3911TBA11', 'V1', '', 1);
insert into Reservation values('R13', '2020-01-21', 'U9', '8956TBB12', 'V2', '', 1);
insert into Reservation values('R14', '2020-01-22', 'U10', '3911TBA13', 'V1', '', 1);
insert into Reservation values('R15', '2020-01-23', 'U4', '8956TBB14', 'V2', '', 1);
insert into Reservation values('R16', '2020-01-24', 'U5', '3911TBA15', 'V1', '', 1);
insert into Reservation values('R17', '2020-01-25', 'U6', '8956TBB16', 'V2', '', 1);
insert into Reservation values('R18', '2020-01-18', 'U7', '3911TBA17', 'V1', '', 1);
insert into Reservation values('R19', '2020-01-17', 'U8', '8956TBB3', 'V2', '', 1);

----------- Extras ------------------------
insert into Extras values('E1', 'eau', 3000, 'une bouteille d`eau');
insert into Extras values('E2', 'petit-dejeuner', 5000, 'petit-dejeuner');
insert into Extras values('E3', 'dejeuner', 8000, 'dejeuner sur route');
insert into Extras values('E4', 'raccompagnement', 15000, 'une equipe vous raccompagnera chez vous');

----------- Extras Reservation ---------------
insert into ExtraReservation values('E1', 'R1');
insert into ExtraReservation values('E2', 'R2');
insert into ExtraReservation values('E1', 'R3');
insert into ExtraReservation values('E3', 'R4');
insert into ExtraReservation values('E1', 'R5');
insert into ExtraReservation values('E4', 'R6');
insert into ExtraReservation values('E2', 'R7');
insert into ExtraReservation values('E3', 'R8');
insert into ExtraReservation values('E1', 'R9');
insert into ExtraReservation values('E2', 'R10');
insert into ExtraReservation values('E3', 'R11');
insert into ExtraReservation values('E4', 'R12');
insert into ExtraReservation values('E1', 'R13');
insert into ExtraReservation values('E1', 'R14');
insert into ExtraReservation values('E1', 'R15');